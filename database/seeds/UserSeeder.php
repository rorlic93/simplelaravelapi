<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Maker;
use App\User;

class UserSeeder extends Seeder{

    public function run(){
        User::create([
            'name' => 'admin',
            'email' => 'admin@admin.hr',
            'password' => Hash::make('admin')
        ]);
    }

}
