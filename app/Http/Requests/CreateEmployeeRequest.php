<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class CreateEmployeeRequest extends Request{
    public function authorize(){
        return true;
    }

    public function rules(){
        return
        [
          'ime_prezime' => 'required'
        ];
    }

    public function response(array $errors){
        return response()->json(['message' => $errors, 'code' => 422], 422);
    }
}
