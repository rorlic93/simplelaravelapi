<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateEmployeeRequest;
use App\Http\Requests\EditEmployeeRequest;
use Illuminate\Support\Facades\DB;
use App\Employee;

class EmployeesController extends Controller
{
    public function __construct(){
      $this->middleware('auth.basic');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        return response()->json($employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CreateEmployeeRequest $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmployeeRequest $request)
    {
        $values = $request->all();
        $employee = new Employee;
        $employee->ime_prezime = $request->input('ime_prezime');

        $employee->save();

        return response()->json(['message' => "Zaposlenik je uspješno kreiran. ID: {$employee->id}"], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::where('id', '=', $id)->get();
        return response()->json(['zaposlenici' => $employee]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditEmployeeRequest $request, $id)
    {
      $nameToChange = $request->input('ime_prezime');

      DB::table('zaposlenici')
                      ->where('id', $id)
                      ->update(['ime_prezime' => $nameToChange]);


      return response()->json(['message' => "Zaposleniku je uređeno ime i prezime. ID: {$id}"], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::where('id', '=', $id)->delete();


        return response()->json(['message' => "Zaposleniku uspješno obrisan iz baze"], 200);
    }
}
