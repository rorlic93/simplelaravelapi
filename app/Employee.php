<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model{

  protected $table = 'zaposlenici';
  protected $primaryKey = 'id';

  protected $fillable = ['ime_prezime'];

}
